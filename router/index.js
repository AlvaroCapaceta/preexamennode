import express from 'express';
export const router = express.Router();
import bodyParser from 'body-parser';

router.get('/', (req, res) => {
  const params = {
    numBoleto: req.query.numBoleto,
    destino: req.query.destino,
    nombre: req.query.nombre,
    edad: req.query.edad,
    viaje: req.query.viaje,
    precio: req.query.precio,
  };

  res.render('form.html', params);
});

router.post('/', (req, res) => {
  const params = {
    numBoleto: req.body.numBoleto,
    destino: req.body.destino,
    nombre: req.body.nombre,
    edad: req.body.edad,
    viaje: req.body.viaje,
    precio: req.body.precio,
  };
  console.log(params);
  res.render('form.html', params);
});
